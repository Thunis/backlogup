use std::env;
use std::fs;
use std::path::Path;
use std::fs::File;
use std::io::{Read,Write};

fn main() {
    let mut args_it = env::args();
    args_it.next(); // skip own program name
    let file_to_backup_path = args_it.next().expect("First argument must be file to backup.");
    let file_to_backup = Path::new(& file_to_backup_path);
    let backup_directory_path = args_it.next().expect("Second argument must be directory to backup to.");
    let backup_directory = Path::new(& backup_directory_path);
    assert!(args_it.next().is_none(), "Unrecognised additional arguments given");
    let file_meta = fs::metadata(&file_to_backup).expect("File to backup not found or inaccessible");
    let dir_meta = fs::metadata(&backup_directory).expect("Backup directory not found or inaccessible");
    assert!(file_meta.is_file(), "File to backup is not really a file.");
    assert!(dir_meta.is_dir(), "The backup directory is not a directory.");
    let metafile_path = backup_directory.join(".last_backup");
    let mod_time_err_string = "File modification times not available. Please implement a data diff approach.";
    let last_update = fs::metadata(&metafile_path).ok().map(|x| x.modified().expect(mod_time_err_string));
    let must_backup = last_update.map(|x| x < file_meta.modified().expect(mod_time_err_string)).unwrap_or(true);
    if must_backup {
        let old_backup_number = File::open(&metafile_path).map(|mut f| {
                let mut s = String::new();
                f.read_to_string(&mut s).expect("Could not read from '.last_backup' file in the backup directory");
                u64::from_str_radix(s.trim(),10).expect("Invalid '.last_backup' file in the backup directory")
            }).unwrap_or(0u64);
        let new_backup_number = old_backup_number + 1;
        let backup_dst = backup_directory.join(format!("{}",new_backup_number));
        assert!(fs::metadata(&backup_dst).is_err(), "Invalid state: The backup file to be written already exists!");
        fs::copy(file_to_backup, backup_dst).expect("Could not copy the file to the backup directory.");
        File::create(&metafile_path).expect("Backup done, but could not open '.last_backup' file for writing.").write_fmt(format_args!("{}", new_backup_number)).expect("Backup done, but could not write to '.last_backup' file.");
    }
}
