`backlogup` is a very simple, non-overwriting backup tool.
It check if a specific file has changed. If so, it copies it to a backup directory, using the next free integer as a file name (1, 2, 3...)
It doesn't delete old copies, but isn't offended if they are deleted by another program.
Just don't delete the `.last_backup` file in the backup directory, so it knows which integer to use next.

Don't be afraid to look at the code.
Seriously, it's like 30 lines.

For usage on a server, a static build is recommended, obtainable using this command:

`cargo build --release --target=x86_64-unknown-linux-musl`

## Usage

`backlogup /path/to/file/to/monitor /path/to/backup/directory`
